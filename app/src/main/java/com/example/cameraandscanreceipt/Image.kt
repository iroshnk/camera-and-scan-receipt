package com.example.cameraandscanreceipt

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat

class Image : AppCompatActivity() {

    private val REQUEST_IMAGE_GALLERY =1
    private val REQUEST_IMAGE_CAMERA =2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image)

        findViewById<ImageView>(R.id.imageView2).setOnClickListener {
            val builder = AlertDialog.Builder(this)
            builder.setTitle("Select Receipt")
            builder.setMessage("Choose your option?")


            builder.setPositiveButton("Gallery") { dialog: DialogInterface, which :Int ->
                dialog.dismiss()

                val intent = Intent(Intent.ACTION_PICK)
                intent.type = "image/*"
                startActivityForResult(intent, REQUEST_IMAGE_GALLERY)

            }
            builder.setNegativeButton("Camera"){ dialog: DialogInterface, which :Int ->
                dialog.dismiss()

                /*
                 Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent: Intent ->
                     takePictureIntent.resolveActivity(packageManager)?.also {
                         val permission :Int = ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA)
                         if (permission != PackageManager.PERMISSION_GRANTED) {
                             ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), 1)
                         }
                         else{
                             startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAMERA)
                         }
                     }
                 }

                 */

                 val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                 try {
                     startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAMERA)
                 } catch (e: ActivityNotFoundException) {
                    // display error state to the user
                 }

            }

            val dialog :AlertDialog = builder.create()
            dialog.show()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_IMAGE_GALLERY && resultCode == Activity.RESULT_OK && data != null){
            findViewById<ImageView>(R.id.imageView2).setImageURI(data.data)
        }
        else if (requestCode == REQUEST_IMAGE_CAMERA && resultCode == Activity.RESULT_OK && data != null){
            findViewById<ImageView>(R.id.imageView2).setImageBitmap(data.extras!!.get("data") as Bitmap)
        }
        else if (resultCode == Activity.RESULT_OK){
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_SHORT).show()
        }
    }
}